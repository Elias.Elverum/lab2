package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {    
    private int max_size = 20;

    private List<FridgeItem> fridgeList;

    public Fridge() {
        fridgeList = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return fridgeList.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()){
            fridgeList.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        for (FridgeItem i : fridgeList){
            if (i == item){
                fridgeList.remove(item);
                return;
            }
        }
        throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        fridgeList.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredList = new ArrayList<FridgeItem>(max_size);
        for (FridgeItem i : fridgeList){
            if (i.hasExpired()){
                expiredList.add(i);
            }
        }
        fridgeList.removeAll(expiredList);
        return expiredList;
    }
}